# table-data
Takes an array of objects (or its JSON equivalent) and returns an object with header and row data.

Usage:

```es6
import tableData from 'table-data'

const data = [
  {
    city: 'Shanghai'
  , nation: 'China'
  , population: 24256800
  }
, {
    city: 'Karachi'
  , population: 23500000
  , nation: 'Pakistan'
  , elevation: '8 m'
  }
, {
    city: 'Beijing'
  , nation: 'China'
  , population: 21516000
  }
, {
    city: 'São Paulo'
  , nation: 'Brazil'
  , population: 12038175
  , mayor: 'João Doria'
  }
, {
    city: 'Dhaka'
  , population: 16970105
  , nation: 'Bangladesh'
  }
]

const formattedData = tableData(data)

// formattedData will look like this:
//
// {
//   header: ["city", "nation", "population", "elevation", "mayor"]
// , rows: [
//     ["Shanghai", "China", 24256800, "", ""]
//   , ["Karachi", "Pakistan", 23500000, "8 m", ""]
//   , ["Beijing", "China", 21516000, "", ""]
//   , ["Sao Paulo", "Brazil", 12038175, "", "João Doria"]
//   , ["Dhaka", "Bangladesh", 16970105, "", ""]
//  ]
// }

```
