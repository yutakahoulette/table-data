import R from 'ramda'

const parseIfJSON = data => {
  // if data is JSON, then parse. 
  // if not, then return
  try { JSON.parse(data) } 
  catch (e) { return data }
  return JSON.parse(data)
}

const nest = data => Array.isArray(data) ? data : [data]

const getHeaderAndRows = data => {
  const header = R.compose(
      R.keys
    , R.reduce((acc, obj) => {
        R.map(x => acc[x] = true, R.keys(obj))
        return acc
      }, {})
    )(data)
  const rows = R.reduce((parentAcc, obj) => {
    const row = R.reduce(
      (childAcc, x) => {
        childAcc.push(obj[x] || '')
        return childAcc
      }
    , [], header)
    parentAcc.push(row)
    return parentAcc
  }, [], data)
  return {header, rows}
}

module.exports = data => 
   R.compose(
    getHeaderAndRows
  , nest
  , parseIfJSON
  )(data)

