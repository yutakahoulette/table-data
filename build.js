'use strict';

var _ramda = require('ramda');

var _ramda2 = _interopRequireDefault(_ramda);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var parseIfJSON = function parseIfJSON(data) {
  // if data is JSON, then parse. 
  // if not, then return
  try {
    JSON.parse(data);
  } catch (e) {
    return data;
  }
  return JSON.parse(data);
};

var nest = function nest(data) {
  return Array.isArray(data) ? data : [data];
};

var getHeaderAndRows = function getHeaderAndRows(data) {
  var header = _ramda2.default.compose(_ramda2.default.keys, _ramda2.default.reduce(function (acc, obj) {
    _ramda2.default.map(function (x) {
      return acc[x] = true;
    }, _ramda2.default.keys(obj));
    return acc;
  }, {}))(data);
  var rows = _ramda2.default.reduce(function (parentAcc, obj) {
    var row = _ramda2.default.reduce(function (childAcc, x) {
      childAcc.push(obj[x] || '');
      return childAcc;
    }, [], header);
    parentAcc.push(row);
    return parentAcc;
  }, [], data);
  return { header: header, rows: rows };
};

module.exports = function (data) {
  return _ramda2.default.compose(getHeaderAndRows, nest, parseIfJSON)(data);
};
