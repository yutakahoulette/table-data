import data from './data'
import table from '../index'

describe('table data', () => {

  it('all header data gets set', () => {
    const header = table(data).header
    expect(header).toEqual(['city', 'nation', 'population', 'elevation', 'mayor'])
  })

  it('rows with missing data contain empty strings', () => {
    expect(table(data).rows[0][3]).toEqual('')
  })

  it('renders JSON and regular JS data the same', () => {
    expect(table(data)).toEqual(table(JSON.stringify(data)))
  })

  it('nests data if it is just an object', () => {
    const d = [
      {
        city: 'Shanghai'
      , nation: 'China'
      , population: 24256800
      }
    ]
    expect(table(d[0])).toEqual(table(d))
  })

})

