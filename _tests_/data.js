const data = [
  {
    city: 'Shanghai'
  , nation: 'China'
  , population: 24256800
  }
, {
    city: 'Karachi'
  , population: 23500000
  , nation: 'Pakistan'
  , elevation: '8 m'
  }
, {
    city: 'Beijing'
  , nation: 'China'
  , population: 21516000
  }
, {
    city: 'Sao Paulo'
  , nation: 'Brazil'
  , population: 12038175
  , mayor: 'João Doria'
  }
, {
    city: 'Dhaka'
  , population: 16970105
  , nation: 'Bangladesh'
  }
]

module.exports = data

